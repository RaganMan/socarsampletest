import { combineReducers } from 'redux'     
import AuthReducer from './authReducer';
import CarReducer from './carReducer';
import CarListReducer from './carListReducer';
import ActiveCarReducer from './activecarReducer'
import ReservedCarReducer from './reservedcarReducer';


export default combineReducers({
  auth: AuthReducer,
  cars: CarReducer,
  carList: CarListReducer,
  activeCar: ActiveCarReducer,
  resCar: ReservedCarReducer
});