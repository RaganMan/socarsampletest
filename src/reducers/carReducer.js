import {
    CAR_UPDATE,
    CAR_CREATE,
} from '../actions/types';

const INITIAL_STATE ={
    carName:'',
    numberPlate:'',
    carOwner:'',
    price:'',
    ownerPhone:''
};

export default (state = INITIAL_STATE, action) => {
    console.log(action)
    switch (action.type) {
        case CAR_UPDATE:
        return {...state, [action.payload.prop]: action.payload.value};
        
        case CAR_CREATE:
        return INITIAL_STATE;

        default:
        return state;
    }
}