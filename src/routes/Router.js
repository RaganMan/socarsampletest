import React, { Component } from "react";
import { Router, Scene } from "react-native-router-flux";
// import Color from "../config/colors.json";
// import { UtilStyles } from "../style/styles";
import { Image, Platform, View } from "react-native";
import Color from "../config/colors.json";
import Home from '../pages/home/home';
import Cars from '../pages/cars/cars';
import Profile from '../pages/profile/profile';
import Settings from '../pages/settings/settings';
import LoginForm from "../pages/login/LoginForm";
import createCars from "../pages/cars/createCar";
import viewCars from "../pages/cars/viewCars";
import activeReservation from "../pages/cars/activeReservation";
import viewReserved from "../pages/cars/viewReserved";
import Account from "../pages/profile/Account";
import Carfile from "../pages/cars/carfile";
console.disableYellowBox = true;

const TabIconHome = ({ focused }) => {
  return (
    <View>
      <Image
        source={
          focused
            ? require("../images/home_blue_icon.png")
            : require("../images/home_grey_icon.png")
        }
        style={{ height: 25, width: 25 }}
      />
    </View>
  );
};

const TabIconCars = ({ focused }) => {
  return (
    <View>
      <Image
        source={
             require("../images/car.png")
        }
        style={{ height: 25, width: 25 }}
      />
    </View>
  );
};

const TabIconProfile = ({ focused }) => {
  return (
    <View>
      <Image
        source={
          focused
            ? require("../images/user_blue_icon.png")
            : require("../images/user_grey_icon.png")
        }
        style={{ height: 25, width: 25 }}
      />
    </View>
  );
};

const TabIconSettings = ({ focused }) => {
  return (
    <View>
      <Image
        source={
          focused
            ? require("../images/setting_blue_icon.png")
            : require("../images/setting_grey_icon.png")
        }
        style={{ height: 25, width: 25 }}
      />
    </View>
  );
};


const RouterComponent = () => {
  return (
    <Router navigationBarStyle={{ backgroundColor: "white" }}>
      <Scene key="root"  hideNavBar>
     
        <Scene
          key="tabbar"
          tabs={true}
          tabBarStyle={{ backgroundColor: "white" }}
          tabBarPosition={"bottom"}
          activeBackgroundColor={Color.material_grey_50}
          activeTintColor={Color.dark_blue}
          inactiveTintColor={Color.material_grey_600}
          showLabel={true}
          labelStyle={{
            fontFamily: "Lato-Regular",
            fontSize: 9
          }}
          swipeEnabled={false}
        >
        <Scene key= "Home" title="Home" icon={TabIconHome} >
          <Scene 
          key="Home"
          component={Home}
          initial
          />
        </Scene>
        
        <Scene key= "Cars" title="Cars" icon={TabIconCars} >
        <Scene 
          key="Carfile"
          component={Carfile}
          />
          <Scene 
          key="Cars"
          component={Cars}
          />
          <Scene
          key="createCars"
          component={createCars}
          title="Car Create" 
          />
           <Scene
          key="viewCars"
          component={viewCars}
          title="Car List" 
          />
          <Scene
          key="active"
          component={activeReservation}
          title="Active List"
          />
          <Scene
          key="reserved"
          component={viewReserved}
          title="My Reserved" 
          />
        </Scene>
        
        <Scene key= "Profile" title="Profile" icon={TabIconProfile} >
        <Scene 
          key="Account"
          component={Account}
          />
          <Scene 
          key="Profile"
          component={Profile}
          />
          <Scene
          key="login"
          component={LoginForm} 
          />
        </Scene>

        <Scene key= "Settings" title="Settings" icon={TabIconSettings} >
          <Scene 
          key="Settings"
          component={Settings}
          />
        </Scene>

        </Scene>

      </Scene>
    </Router>

  )
};

export default RouterComponent;
