import React, { Component } from 'react';
import {TextInput,View,Text} from 'react-native';

const Input = ({label, value, onChangeText ,placeholder, secureTextEntry}) => {
    const {inputStyle,containerStyle,labelStyle} = styles;
return(
    <View style = {containerStyle}>
   
    <TextInput 
    secureTextEntry = {secureTextEntry}
    placeholder = {placeholder}
    autoCorrect = {false}
    value  ={value}
    onChangeText = {onChangeText} 
    style = {inputStyle}/>
     </View>

);
};

const styles = {
    inputStyle : {
        color: '#000',
        paddingRight: 5,
        paddingLeft: 5,
        fontSize:18,
        lineHeight:23,
        flex:2,
        width:100
    },
   
    containerStyle: {
        height:40,
        flex:1,
        flexDirection: 'row',
      
        alignItems: 'center',
    }
}

export {Input};