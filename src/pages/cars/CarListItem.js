import React, { Component } from 'react';
import { View, Text, Image, ListView, ActivityIndicator, Alert } from "react-native";
import { Card, CardSection, Input, Button, Spinner } from "../../components/common";
import firebase from 'firebase'
import { Actions } from "react-native-router-flux";

class CarListItem extends Component {

    static onEnter = () => {
        Actions.refresh();
      };


    onReserveButtongPress() {
        console.log(this.props.car);
        const { currentUser } = firebase.auth();

        console.log("Current User")
        console.log(currentUser.email);
        var carData = {
            carName: this.props.car.carName,
            numberPlate: this.props.car.numberPlate,
            carOwner: this.props.car.carOwner,
            price: this.props.car.price,
            ownerPhone: this.props.car.ownerPhone
        }
        console.log(carData)

        firebase.database().ref(`/cars/${currentUser.uid}/active`)
            .push(
                carData
            ).then(() => {
                console.log("Success");
                Actions.active({ car: carData });
            })
    }

    render() {
        const { carName, carOwner, numberPlate, ownerPhone, price, uid } = this.props.car;
        return (
            <Card>
                <CardSection>
                    <Text style={styles.titleStyle}>
                        Car Name:
                     </Text>
                    <Text style={styles.titleStyle}>
                        {carName}
                    </Text>
                </CardSection>
                <CardSection>
                    <Text style={styles.otherStyle}>
                        Owner:
                    </Text>
                    <Text style={styles.otherStyle}>
                        {carOwner}
                    </Text>
                </CardSection>
                <CardSection>
                    <Text style={styles.otherStyle}>
                        Number Plate:
                    </Text>
                    <Text style={styles.otherStyle}>
                        {numberPlate}
                    </Text>
                </CardSection>
                <CardSection>
                    <Text style={styles.otherStyle}>
                        Phone:
                    </Text>
                    <Text style={styles.otherStyle}>
                        {ownerPhone}
                    </Text>
                </CardSection>
                <CardSection>
                    <Text style={styles.otherStyle}>
                        Price/Hour:
                    </Text>
                    <Text style={styles.otherStyle}>
                        {price}
                    </Text>
                </CardSection>
                <CardSection>
                    <Button
                        onPress={this.onReserveButtongPress.bind(this)}
                    >
                        Reserve
                </Button>
                </CardSection>
            </Card>
        )
    }
}
const styles = {
    titleStyle: {
        fonstSize: 18,
        paddingLeft: 15,
        color: 'black',
        fontWeight: '600'
    },
    otherStyle: {
        fontSize: 14,
        paddingLeft: 12,
        color: 'black'
    }
}
export default CarListItem;