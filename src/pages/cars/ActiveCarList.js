import React, { Component } from 'react';
import { connect } from 'react-redux'
import { View, Text, Image, ListView, ActivityIndicator, Alert } from "react-native";
import { deactivateCar } from '../../actions'
import { Card, CardSection, Input, Button, Spinner } from "../../components/common";
import firebase from 'firebase'
import { Actions } from "react-native-router-flux";
import { Container, Header, Left, Body, Right, Title } from 'native-base';

class ActiveCarList extends Component {
    onStopButtongPress() {
        const { uid } = this.props.car;
        console.log(uid);
        const { currentUser } = firebase.auth();
        console.log(currentUser.email);
        console.log("Current User")
        console.log(currentUser.email);
        var carData = {
            carName: this.props.car.carName,
            numberPlate: this.props.car.numberPlate,
            carOwner: this.props.car.carOwner,
            price: this.props.car.price,
            ownerPhone: this.props.car.ownerPhone
        }
        console.log(carData)

        firebase.database().ref(`/cars/${currentUser.uid}/myreservation`)
            .push(
                carData
            ).then(() => {
                console.log("Success");
                // Actions.active({car: carData});
                this.props.deactivateCar({ uid })
                Actions.reserved();

            })
    }
    render() {
        const { carName, carOwner, numberPlate, ownerPhone, price, uid } = this.props.car;
        return (
            <Container>
                {/* <Header>
                    <Body>
                        <Title>Active Reservation</Title>
                    </Body>
                </Header> */}
                <Card>
                    <CardSection>
                        <View>
                            <Image
                                source={
                                    require("../../images/active.png")
                                }
                                style={{ height: 25, width: 25 }}
                            />
                            <Text style={{color:'black', fontSize:18, fontWeight:'bold'}}>Active Car</Text>
                        </View>
                    </CardSection>
                    <CardSection>
                        <Text style={styles.titleStyle}>
                            Car Name:
                     </Text>
                        <Text style={styles.titleStyle}>
                            {carName}
                        </Text>
                    </CardSection>
                    <CardSection>
                        <Text style={styles.otherStyle}>
                            Owner:
                    </Text>
                        <Text style={styles.otherStyle}>
                            {carOwner}
                        </Text>
                    </CardSection>
                    <CardSection>
                        <Text style={styles.otherStyle}>
                            Number Plate:
                    </Text>
                        <Text style={styles.otherStyle}>
                            {numberPlate}
                        </Text>
                    </CardSection>
                    <CardSection>
                        <Text style={styles.otherStyle}>
                            Phone:
                    </Text>
                        <Text style={styles.otherStyle}>
                            {ownerPhone}
                        </Text>
                    </CardSection>
                    <CardSection>
                        <Text style={styles.otherStyle}>
                            Price/Hour:
                    </Text>
                        <Text style={styles.otherStyle}>
                            {price}
                        </Text>
                    </CardSection>
                    <CardSection>
                        <Button
                            onPress={this.onStopButtongPress.bind(this)}
                        >
                            Stop Reserving.
                </Button>
                    </CardSection>
                </Card>
            </Container>
        )
    }
}
const styles = {
    titleStyle: {
        fonstSize: 18,
        paddingLeft: 15,
        color: 'black',
        fontWeight: '600'
    },
    otherStyle: {
        fontSize: 14,
        paddingLeft: 12,
        color: 'black'
    }
}

export default connect(null, { deactivateCar })(ActiveCarList);