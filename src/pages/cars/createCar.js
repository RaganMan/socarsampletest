import React, { Component } from 'react';
import { connect } from 'react-redux';
import { carUpdate, carCreate } from '../../actions';
import { View, Text } from 'react-native';

import { Card, CardSection, Input, Button, Spinner } from "../../components/common";

class createCars extends Component {

    onCreateButtonPress() {
        const { carName, numberPlate, carOwner, price, ownerPhone } = this.props;
        this.props.carCreate({ carName, numberPlate, carOwner, price, ownerPhone });
    }

    render() {
        return (
            <Card>
                <CardSection>
                    <Input
                        label="Car Name"
                        placeholder="Toyota Vios"
                        value={this.props.carName}
                        onChangeText={text => this.props.carUpdate({ prop: 'carName', value: text })}
                    />
                </CardSection>

                <CardSection>
                    <Input
                        label="Number Plate"
                        placeholder="WUW 1673"
                        value={this.props.numberPlate}
                        onChangeText={text => this.props.carUpdate({ prop: 'numberPlate', value: text })}
                    />
                </CardSection>

                <CardSection>
                    <Input
                        label="Car Owner"
                        placeholder="Jane"
                        value={this.props.carOwner}
                        onChangeText={text => this.props.carUpdate({ prop: 'carOwner', value: text })}
                    />

                </CardSection>

                <CardSection>
                    <Input
                        label="Price/Hour"
                        placeholder="RM 8"
                        value={this.props.price}
                        onChangeText={text => this.props.carUpdate({ prop: 'price', value: text })}
                    />
                </CardSection>

                <CardSection>
                    <Input
                        label="Owner Phone"
                        placeholder="0172898989"
                        value={this.props.ownerPhone}
                        onChangeText={text => this.props.carUpdate({ prop: 'ownerPhone', value: text })}
                    />
                </CardSection>

                <CardSection>
                    <Button
                        onPress={this.onCreateButtonPress.bind(this)}
                    >
                        Create
                </Button>
                </CardSection>
            </Card>
        )
    }
}

const mapStateToProps = (state) => {
    const { carName, numberPlate, carOwner, price, ownerPhone } = state.cars;
    return { carName, numberPlate, carOwner, price, ownerPhone }
}

export default connect(mapStateToProps, { carUpdate, carCreate })(createCars);