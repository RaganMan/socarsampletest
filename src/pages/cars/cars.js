import React, { Component } from "react";
import { View, Text, Image,  AsyncStorage,Alert } from "react-native";
// import { UtilStyles } from "../style/styles";
import { Actions } from "react-native-router-flux";
import LoginForm from "../login/LoginForm";
import { Card, CardSection, Input, Button, Spinner } from "../../components/common";
class Cars extends Component {
   
    static onEnter = () => {
        Actions.refresh();
      };


    constructor(props) {
        super(props);
        this.state = { email: '' };
        this.authenticateSession();
    }

    componentWillMount(){
        this.authenticateSession();
    }

    componentWillReceiveProps(){
        this.authenticateSession();
    }
    
   authenticateSession =  async () => {
        try {
          await AsyncStorage.getItem("loginUser").then((res, error) => {
            if (res) {
              console.log(res)
              this.setState({
                  email:res
              })
            } else {
              console.log("no data");
            }
          });
        } catch (error) {
          console.log(error);
        }
      }

    onCreateButtonPress() {
        Actions.createCars();
    }
    onViewButtonPress(){
            
            if(!this.state.email){
                Alert.alert(
                    'Title',
                    'Please Login to Reserve a car',
                    [
                        { text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
                        {
                            text: 'OK', onPress: () => {
                                Actions.login();
                            }
                        },
                    ],
                    { cancelable: false }
                )
            }
            else{
                Actions.viewCars();
     
            }
            
    }
    onReservedButtonPress(){
            
        if(!this.state.email){
            Alert.alert(
                'Title',
                'Please Login to Reserve a car',
                [
                    { text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
                    {
                        text: 'OK', onPress: () => {
                            Actions.login();
                        }
                    },
                ],
                { cancelable: false }
            )
        }
        else{
            Actions.reserved();
        }
         
     
    }
    renderCreateCarListButton() {
        return (
            <Button
                onPress={this.onCreateButtonPress.bind(this)}
            >
                Create Car List
            </Button>
        )
    }

    renderSelectCarButton() {
        return (
            <Button
            onPress={this.onViewButtonPress.bind(this)}
            >
                Select Car To Reserve
            </Button>
        )
    }

    renderReservedCar() {
        return (
            <Button
            onPress={this.onReservedButtonPress.bind(this)}
            >
                See Reservation
            </Button>
        )
    }

    renderArea(){
        if(!this.state.email){
            return(
                <LoginForm text="cars"/>
            )
        }
        else{
            return(
                <Card>
                <CardSection>
                    {this.renderCreateCarListButton()}
                </CardSection>
                <CardSection>
                    {this.renderSelectCarButton()}
                </CardSection>
                <CardSection>
                    {this.renderReservedCar()}
                </CardSection>
            </Card>

            )
        }
    }


    render() {
       
        return (
          <View>
              {this.renderArea()}
              </View>
        )
    }
}

export default Cars;