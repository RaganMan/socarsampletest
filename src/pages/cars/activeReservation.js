import React, { Component } from 'react';
import _ from 'lodash';
import { connect } from 'react-redux'
import { activecarFetch } from '../../actions'
import { View, Text, Image, ListView, ActivityIndicator, Alert } from "react-native";
import { Card, CardSection, Input, Button, Spinner } from "../../components/common";
import firebase from 'firebase'
import { Actions } from "react-native-router-flux";
import ActiveCarList from './ActiveCarList';
class activeReservation extends Component {

    componentWillMount() {
        this.props.activecarFetch();
        //console.log(this.props.car)
        this.createDataSource(this.props)
    }

    componentWillReceiveProps(nextProps) {
        this.createDataSource(nextProps)
    }

    createDataSource({ activecar }) {

        const ds = new ListView.DataSource({
            rowHasChanged: (r1, r2) => r1 !== r2
        });

        this.dataSource = ds.cloneWithRows(activecar)
    }

    renderRow(car){
        return <ActiveCarList car = {car} />;
    }

    render() {
        console.log(this.props.activecar)
        if(this.props.activecar.length === 0 ){
            return(
                <View>
                    <Text>
                        No Cars Activated
                    </Text>
                </View>
            )
        }
        return (
            <ListView
            enableEmptySections
            dataSource = {this.dataSource}
            renderRow ={this.renderRow}
            />
        )
    }
}

const mapStateToProps = state => {
    const activecar = _.map(state.activeCar, (val, uid) => {
        return { ...val, uid };
    });
    return { activecar };
};

export default connect(mapStateToProps, { activecarFetch })(activeReservation);