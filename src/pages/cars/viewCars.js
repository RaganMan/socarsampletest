import _ from 'lodash';
import React, {Component} from 'react';
import { View, Text, Image, ListView,ActivityIndicator } from "react-native";
import {connect} from 'react-redux';
import{carFetch} from '../../actions';
import CarListItem from './CarListItem';
import { Card, CardSection, Input, Button, Spinner } from "../../components/common";
import { validateArgCount } from '@firebase/util';
import { ListItem } from 'react-native-elements';

class viewCars extends Component{
    componentWillMount(){
        this.props.carFetch();

        this.createDataSource(this.props)
    }

    componentWillReceiveProps(nextProps){
        this.createDataSource(nextProps)
    }

    createDataSource({carlist}){

        const ds = new ListView.DataSource({
            rowHasChanged: (r1,r2) => r1 !== r2
        });

        this.dataSource = ds.cloneWithRows(carlist)
    }

    renderRow(car){
        return <CarListItem car = {car} />;

    }

    render(){
        console.log(this.props.carlist)
        if(this.props.carlist.length === 0){
            return(
                <View style={{alignSelf: 'center'}}>
                <ActivityIndicator size ="large" />
                </View>
            )
        }
        return(
            <ListView
            enableEmptySections
            dataSource = {this.dataSource}
            renderRow ={this.renderRow}
            />
        )
    }
}

const mapStateToProps = state =>{
const carlist = _.map(state.carList, (val, uid) =>{
    return{...val, uid};
});
return {carlist};
};

export default connect(mapStateToProps,{carFetch})(viewCars);