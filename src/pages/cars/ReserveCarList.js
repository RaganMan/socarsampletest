import React, { Component } from 'react';
import { View, Text, Image, ListView, ActivityIndicator, Alert } from "react-native";
import { Card, CardSection, Input, Button, Spinner } from "../../components/common";
import firebase from 'firebase'
import { Actions } from "react-native-router-flux";

class ReserveCarList extends Component {
  
    render() {
        const { carName, carOwner, numberPlate, ownerPhone, price, uid } = this.props.car;
        return (
            <Card>
                <CardSection>
                    <Text style={styles.titleStyle}>
                        Car Name:
                     </Text>
                    <Text style={styles.titleStyle}>
                        {carName}
                    </Text>
                </CardSection>
                <CardSection>
                    <Text style={styles.otherStyle}>
                        Owner:
                    </Text>
                    <Text style={styles.otherStyle}>
                        {carOwner}
                    </Text>
                </CardSection>
                <CardSection>
                    <Text style={styles.otherStyle}>
                        Number Plate:
                    </Text>
                    <Text style={styles.otherStyle}>
                        {numberPlate}
                    </Text>
                </CardSection>
                <CardSection>
                    <Text style={styles.otherStyle}>
                        Phone:
                    </Text>
                    <Text style={styles.otherStyle}>
                        {ownerPhone}
                    </Text>
                </CardSection>
                <CardSection>
                    <Text style={styles.otherStyle}>
                        Price/Hour:
                    </Text>
                    <Text style={styles.otherStyle}>
                        {price}
                    </Text>
                </CardSection>
            </Card>
        )
    }
}
const styles = {
    titleStyle: {
        fonstSize: 18,
        paddingLeft: 15,
        color: 'black',
        fontWeight: '600'
    },
    otherStyle: {
        fontSize: 14,
        paddingLeft: 12,
        color: 'black'
    }
}
export default ReserveCarList;