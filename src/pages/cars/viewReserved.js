import React, { Component } from 'react';
import _ from 'lodash';
import { connect } from 'react-redux'
import { View, Text, Image, ListView, ActivityIndicator, Alert } from "react-native";
import {viewReserved} from '../../actions'
import { Card, CardSection, Input, Button, Spinner } from "../../components/common";
import firebase from 'firebase'
import { Actions } from "react-native-router-flux";
import { Container, Header, Left, Body, Right, Title } from 'native-base';
import ReserveCarList from './ReserveCarList';

class ViewReserved extends Component{

componentWillMount() {
        this.props.viewReserved();
        //console.log(this.props.car)
        this.createDataSource(this.props)
    }
    
    componentWillReceiveProps(nextProps) {
        this.createDataSource(nextProps)
    }

    createDataSource({ rescar }) {

        const ds = new ListView.DataSource({
            rowHasChanged: (r1, r2) => r1 !== r2
        });

        this.dataSource = ds.cloneWithRows(rescar)
    }

    renderRow(car){
        return <ReserveCarList car = {car} />;
    }


    render(){
        if(this.props.rescar.length === 0){
            return(
                <View style={{alignSelf: 'center'}}>
                <ActivityIndicator size ="large" />
                </View>
            )
        }
        return(
             <ListView
            enableEmptySections
            dataSource = {this.dataSource}
            renderRow ={this.renderRow}
            />
        )
    }
}
const mapStateToProps = state => {
    const rescar = _.map(state.resCar, (val, uid) => {
        return { ...val, uid };
    });
    return { rescar };
};
export default connect (mapStateToProps,{viewReserved})(ViewReserved);