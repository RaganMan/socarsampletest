import React, { Component } from 'react';
import { View, Text, Image, ListView, ActivityIndicator, Alert, AsyncStorage } from "react-native";
import { Card, CardSection, Input, Button, Spinner } from "../../components/common";
import firebase from 'firebase'
import { Actions } from "react-native-router-flux";

class Profile extends Component{
    onReservedButtonPress(){
        Actions.reserved();
    }
    onActiveButtonPress(){
        Actions.active();
    }
    onLogOutButtonPress = async()=>{
        firebase.auth().signOut()
        await AsyncStorage.removeItem("loginUser");
        Actions.login();
    }
    renderLogOut(){
        return(
            <Button
            onPress = {this.onLogOutButtonPress.bind(this)}>
                LogOut
            </Button>
        )
    }
    renderActiveCar(){
        return (
            <Button
            onPress={this.onActiveButtonPress.bind(this)}
            >
                See Active
            </Button>
        )
    }
    renderReservedCar() {
        return (
            <Button
            onPress={this.onReservedButtonPress.bind(this)}
            >
                See Reservation
            </Button>
        )
    }
    render(){
        console.log(this.props.user)

        return(
           <Card>
               <CardSection>
                   <Text>
                       {this.props.user}
                   </Text>
                </CardSection>
                
               <CardSection>
               {this.renderReservedCar()}
                </CardSection>
                <CardSection>
               {this.renderActiveCar()}
                </CardSection>
                <CardSection>
               {this.renderLogOut()}
                </CardSection>
            </Card>
        )
    }
}

export default Profile;