import React, { Component } from "react";
import { View, Text, Image, AsyncStorage,ScrollView,RefreshControl } from "react-native";
// import { UtilStyles } from "../style/styles";
import LoginForm from "../login/LoginForm";
import ProfilePage from './profilePage';
// import firebase from 'firebase';
import { connect } from 'react-redux';
import { loginUser } from '../../actions';
class Profile extends Component{
    static onEnter = () => {
        Actions.refresh();
      };

    constructor(props) {
        super(props);
        this.state = { email: '' ,
        refreshing: false};
      }

      componentWillMount(){
        this.authenticateSession();
    }
    
    componentWillReceiveProps(){
        this.authenticateSession();
    }

    // componentDidUpdate(){
    //   this.authenticateSession();
    // }
    
    
      authenticateSession =  async () => {
        try {
          await AsyncStorage.getItem("loginUser").then((res, error) => {
            if (res) {
              console.log(res)
              this.setState({
                  email:res
              })
            } else {
              console.log("no data");
            }
          });
        } catch (error) {
          console.log(error);
        }
      }

    
      renderProfile(){
          if(!this.state.email){
              return(
                
                  <LoginForm text="profilePage"/>
              )
          }
          else{
              return(
                
                  <ProfilePage user = {this.state.email} />
              )
          }
      }

    render(){
        
        return(
            // <LoginForm />
            <View>
            {this.renderProfile()}
            </View>
        )
    }
}

const mapStateToProps = state => {
  return {
      log:state.auth.log,
      error: state.auth.error,
      loading: state.auth.loading
  };
};

export default connect(mapStateToProps, {  loginUser }) (Profile);