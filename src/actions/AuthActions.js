import firebase from 'firebase';
import {AsyncStorage} from 'react-native';
require('firebase/auth')
import {
    EMAIL_CHANGED,
    PASSWORD_CHANGED,
    LOGIN_USER_FAILURE,
    LOGIN_USER_SUCCESS,
    LOGIN_USER
} from './types';
import { Actions } from 'react-native-router-flux';

export const emailChanged = (text) => {
    return {
        type: EMAIL_CHANGED,
        payload: text
    }
}

export const passwordChanged = (text) => {
    return {
        type: PASSWORD_CHANGED,
        payload: text
    }
}

export const loginUser = ({ email, password }, a) => {
    return (dispatch) => {
        dispatch({
            type: LOGIN_USER
        })

        firebase.auth().signInWithEmailAndPassword(email, password)
            .then((res) => {
                loginUserSuccess(dispatch, res, a);
                //console.log(res.user.email)
            })
            .catch((err) => {
                console.log(err)
                firebase.auth().createUserWithEmailAndPassword(email, password)
                    .then(user => {
                        loginUserSuccess(dispatch, user,a);
                    })
                    .catch(() => {
                        loginUserFail(dispatch)
                    })
            })
    }

}

const loginUserFail = (dispatch) => {
    dispatch({
        type: LOGIN_USER_FAILURE
    })
}

const loginUserSuccess = async(dispatch, user, a) => {
    console.log("USer")
    console.log(user.email);
    console.log(a)
    dispatch({
        type: LOGIN_USER_SUCCESS,
        payload: user
    })
   
    await AsyncStorage.setItem("loginUser", user.email).then(function(res, a) {
        console.log(res);
        console.log(a)
        console.log("succccesss");
        
      });

      if (a === "profilePage") {
        Actions.Account({ page: "login" });
      }
      if (a === "cars") {
        Actions.Cars({ page: "login" });
      }

     
}
