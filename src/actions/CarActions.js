import firebase from 'firebase'
import { Actions } from "react-native-router-flux";
import { CAR_UPDATE, CAR_CREATE, CARS_FETCH_SUCCESS, ACTIVECAR_FETCH_SUCCESS,RESERVEDCAR_FETCH_SUCCESS } from './types';

export const carUpdate =({prop, value}) => {
return{
    type:CAR_UPDATE,
    payload:{ prop, value }
}
};

export const carCreate = ({ carName, numberPlate, carOwner, price, ownerPhone }) => {
    console.log("Details");
    console.log(carName, numberPlate, carOwner, price, ownerPhone);
    var carData ={
        carName: carName,
        numberPlate:numberPlate,
        carOwner:carOwner,
        price:price,
        ownerPhone:ownerPhone
    }
    return(dispatch) => {
        firebase.database().ref('/cars/carList')
        .push(carData)
        .then(()=>{
            dispatch({
                type:CAR_CREATE
            });
            console.log('Succesfull')
            Actions.pop();
        });
    }
    
}

export const carFetch = () =>{
  return(dispatch) =>{
      firebase.database().ref('/cars/carList')
      .on('value', snapshot => {
            dispatch({
                type:CARS_FETCH_SUCCESS,
                payload:snapshot.val()
            });
      })
  }   
};

export const activecarFetch = () => {
const{ currentUser } = firebase.auth();
    return(dispatch) => {
        firebase.database().ref(`/cars/${currentUser.uid}/active`)
        .on('value', snapshot => {
            dispatch({
                type:ACTIVECAR_FETCH_SUCCESS,
                payload: '' || snapshot.val()
            });
        })
    }
};


export const deactivateCar = ({uid}) => {

    const{ currentUser } = firebase.auth();
    return() => {
    firebase.database().ref(`/cars/${currentUser.uid}/active/${uid}`)
    .remove()
    .then(() => {
        console.log("Deactive Success")
    })
    }
};

export const viewReserved = () => {
    const{ currentUser } = firebase.auth();
    return(dispatch) => {
        firebase.database().ref(`/cars/${currentUser.uid}/myreservation`)
        .on('value', snapshot => {
            dispatch({
                type:RESERVEDCAR_FETCH_SUCCESS,
                payload:snapshot.val()
            });
        })
    }
}