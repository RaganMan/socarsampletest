import React, { Component } from "react";
import { Provider } from "react-redux";
import { createStore, applyMiddleware } from "redux";
import reducers from "../src/reducers";
import ReduxThunk from "redux-thunk";
import {Platform, StyleSheet, Text, View} from 'react-native';
// import Router from "./routes/Router";
// import firebase from '@firebase/app'

import firebase from 'firebase'
import Home from '../src/pages/home/home';
import Router from './routes/Router';

class App extends Component {

  componentWillMount() {
    console.log("Component Mount From APp.js");
    const config = {
        apiKey: "AIzaSyCI7AsKIPnHeULBFhjB8c8kABTuTSBauIY",
        authDomain: "socar-2a2a2.firebaseapp.com",
        databaseURL: "https://socar-2a2a2.firebaseio.com",
        projectId: "socar-2a2a2",
        storageBucket: "socar-2a2a2.appspot.com",
        messagingSenderId: "921150857446"
      };
      firebase.initializeApp(config);
      console.log(firebase)
  }

  render() {
    const store = createStore(reducers, {}, applyMiddleware(ReduxThunk));

    return (
      <Provider store={store}>
       <Router />
      </Provider>
    );
  }
}

export default App;